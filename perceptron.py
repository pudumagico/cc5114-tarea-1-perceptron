import unittest

class perceptron:
    def __init__(self, w1, w2, bias):
        self.w1 = w1
        self.w2 = w2
        self.bias = bias

    def set_weight(self, w1, w2):
        self.w1 = w1
        self.w2 = w2

    def set_bias(self, bias):
        self.bias = bias

    def output(self, x1, x2):
        if x1*self.w1 + x2*self.w2 + self.bias>0:
            return 1
        else:
            return 0
class nn_2bit_sum:
    def calc_sum(x1, x2):
        p_nand = perceptron(-2, -2, 3)

        output_p_init = p_nand.output(x1, x2)
        output_p_middle1 = p_nand.output(x1, output_p_init)
        output_p_middle2 = p_nand.output(x2, output_p_init)
        output_p_carry = p_nand.output(output_p_init, output_p_init)
        output_p_sum = p_nand.output(output_p_middle1, output_p_middle2)

        return [output_p_sum, output_p_carry]

class PerceptronTestCase(unittest.TestCase):
    def runTest(self):
        p = perceptron(2, 2, -3)
        assert (p.output(0,0)==0 and p.output(1,0)==0 and p.output(0,1)==0 and p.output(1,1)==1) == True, 'percetron AND fail'

        p.set_weight(2, 2)
        p.set_bias(-1)
        assert (p.output(0,0)==0 and p.output(1,0)==1 and p.output(0,1)==1 and p.output(1,1)==1) == True, 'percetron OR fail'

        p.set_weight(-2, -2) 
        p.set_bias(3)
        assert (p.output(0,0)==1 and p.output(1,0)==1 and p.output(0,1)==1 and p.output(1,1)==0) == True, 'percetron NAND fail'


        assert (nn_2bit_sum.calc_sum(0,0) == [0, 0] and nn_2bit_sum.calc_sum(1,0) == [1, 0] and nn_2bit_sum.calc_sum(0,1) == [1, 0] and nn_2bit_sum.calc_sum(1, 1) == [0, 1]) , 'percetron SUM fail'




if __name__ == "__main__":
    unittest.main()
